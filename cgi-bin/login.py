#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cgi, cgitb, hashlib, Cookie, sqlite3

# Para mostrar errores CGI
cgitb.enable()

# Sacamos los datos del formulario
form = cgi.FieldStorage()
post_user = form.getfirst('user', '')
post_password = form.getfirst('password', '')

# Ciframos la contraseña
passwd = hashlib.sha1()
passwd.update(post_password)
post_password = passwd.hexdigest()

with sqlite3.connect("agenda.db") as connection:
  c = connection.cursor()
  c.execute("SELECT * FROM users WHERE username=\"" + post_user + "\"")

  users = c.fetchall()

  if not users:
    print "Refresh: 0; url=/\n"
  else:
    username = users[0][1]
    password = users[0][2]

    if post_user == username and post_password == password:
      thiscookie = Cookie.SimpleCookie()
      thiscookie['logged_in'] = True
      print thiscookie
      print "Refresh: 0; url=/cgi-bin/blog.py\r\n"
    else:
      print "Refresh: 0; url=/\n"