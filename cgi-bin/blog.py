#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cgi, cgitb, Cookie, sqlite3, os

# Para mostrar errores CGI
cgitb.enable()

# HTTP Header
print "Content-Type: text/html"

# Comprobamos si el usuario esta autentificado mediante una Cookie
logged_in = False
thiscookie = Cookie.SimpleCookie()

if os.environ.has_key('HTTP_COOKIE'):
  thiscookie.load(os.environ['HTTP_COOKIE'])
if 'logged_in' in thiscookie:
  logged_in = bool(thiscookie['logged_in'].value)

# Mandar a la página de login si no esta autentificado
if not logged_in:
  print "Refresh: 0; url=/"

# final cabecera
print

# Datos del formulario
form = cgi.FieldStorage()
firstname = form.getfirst('firstname', '')
lastname = form.getfirst('lastname', '')
email = form.getfirst('email', '')

# Nos conectamos a la base de datos
with sqlite3.connect("agenda.db") as connection:
  # Para poder usar 8-bit encuenta de unicode para string
  connection.text_factory = str

  c = connection.cursor()

  # Si hay datos los añado
  if firstname != "" and lastname != "" and email != "":
    c.execute('INSERT INTO contacts VALUES(?, ?, ?)', (firstname, lastname, email))

  # Numero de contactos
  c.execute("SELECT COUNT(*) FROM contacts")
  total = c.fetchone()[0]

  # Seleccionar los contactos
  c.execute("SELECT * FROM contacts ORDER BY lastname")
  contacts = c.fetchall()

  # Sacar las columnas a pantalla
  lista_contactos = '<ul class="contacts">'
  for c in contacts:
    linea ='<li><span class="name">%s %s</span> <span class="email">%s</span></li>' % (c[0], c[1], c[2])
    lista_contactos += linea
  lista_contactos += '</ul>'

  # Creamos el HTML para mas tarde mostrarlo
  doc_html = '''
    <!doctype html>
    <html>
      <head>
        <title>Agenda</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="../css/style.css">
      </head>
      <body>
        <div class="wrapper">
          <header>
            <h1>Mi agenda</h1>
            <p>{total} contactos guardados</p>
            <form method="POST" action="/cgi-bin/logout.py">
              <input type="submit" value="Logout">
            </form>
          </header>

          {lista}

          <div class="addcontact">
            <form method="POST" action="/cgi-bin/blog.py">
              <label>Nombre:</label>
              <input type="text" name="firstname" value="">
              <label>Apellido:</label>
              <input type="text" name="lastname" value="">
              <label>Correo:</label>
              <input type="text" name="email" value="">
              <input type="submit" value="Guardar">
            </form>
          </div>
        </div>
      </body>
    </html>
  '''

  # Mostramos el HTML por pantalla
  print doc_html.format(total=total, lista=lista_contactos)