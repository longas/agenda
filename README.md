Agenda con CGI
==============
Gabriel Longás
--------------

### Funcionamiento
* Ir a la carpeta donde estan los archivos y arrancar el servidor CGI (**$ python -m CGIHTTPServer**)
* En el navegador ir a **localhost:8000**
* Nos podemos registrar o usar el usuario ya creado de admin cuya password es admin también
* En la siguiente pantalla podrá ver el listado de contactos y añadir mas