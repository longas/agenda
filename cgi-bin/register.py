#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cgi, cgitb, hashlib, Cookie, sqlite3

# Para mostrar errores CGI
cgitb.enable()

# Sacamos los datos del formulario
form = cgi.FieldStorage()
post_user = form.getfirst('user', '')
post_password = form.getfirst('password', '')

# Ciframos la contraseña
passwd = hashlib.sha1()
passwd.update(post_password)
post_password = passwd.hexdigest()

# Nos conectamos a la base de datos
with sqlite3.connect("agenda.db") as connection:
  # Para poder usar 8-bit encuenta de unicode para string
  connection.text_factory = str

  c = connection.cursor()

  # Si hay datos los añado
  if post_user != "" and post_password != "":
    c.execute('INSERT INTO users(username, password) VALUES(?, ?)', (post_user, post_password))
  else:
    print "Refresh: 0; url=/\n"

  # Guardamos cookie y redirigimos
  thiscookie = Cookie.SimpleCookie()
  thiscookie['logged_in'] = True
  print thiscookie
  print "Refresh: 0; url=/cgi-bin/blog.py\r\n"